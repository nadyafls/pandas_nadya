#!/usr/bin/env python
# coding: utf-8

# In[2]:


pip install numpy


# In[1]:


pip install pandas


# In[1]:


import numpy as np
import pandas as pd


# In[2]:


#Series
labels = ['a','b','c']
my_list = [10,20,30]
arr = np.array ([10,20,30])
d = {'a':10,'b':20,'c':30}


# In[3]:


pd.Series(data = my_list)


# In[5]:


pd.Series (data = my_list, index = labels)


# In[6]:


pd.Series (my_list,labels)


# In[7]:


pd.Series (arr)


# In[8]:


pd.Series (arr,labels)


# In[9]:


pd.Series (d)


# In[10]:


#data in a series
pd.Series (data = labels)


# In[11]:


#even functions (although unlikely that you will use this)
pd.Series ([sum,print,len])


# In[16]:


#using an index
ser1 = pd.Series ([1,2,3,4] , index = ['USA','Germany','USSR','Japan'])


# In[17]:


ser1


# In[18]:


ser2 = pd.Series ([1,2,5,4], index = ['USA','Germany','Italy','Japan'])


# In[19]:


ser2


# In[20]:


ser1['USA']


# In[21]:


ser1 + ser2


# In[22]:


#data frame


# In[23]:


import pandas as pd
import numpy as np


# In[25]:


from numpy.random import randn
np.random.seed (101)


# In[26]:


df = pd.DataFrame (randn(5,4), index = 'A B C D E'.split(), columns = 'W X Y Z'.split())


# In[27]:


df


# In[28]:


#Selection and Indexing
df ['W']


# In[29]:


# Pass a list of column names
df [['W','Z']]


# In[30]:


#SQL Syntax (NOT RECOMENDED)
df.W


# In[33]:


type (df['W'])


# In[34]:


df['new'] = df ['W'] + df['Y']


# In[35]:


df


# In[37]:


df.drop ('new',axis = 1)


# In[38]:


# Not inplace unless specified!
df


# In[39]:


df.drop ('new',axis = 1,inplace = True)


# In[40]:


df


# In[41]:


df.drop ('E', axis = 0)


# In[42]:


df.loc ['A']


# In[43]:


df.iloc[2]


# In[44]:


df.loc ['B','Y']


# In[45]:


df.loc [['A','B'],['W','Y']]


# In[47]:


#Conditional Selection
df


# In[48]:


df > 0


# In[49]:


df [df >0 ]


# In[50]:


df [df['W']>0]


# In[51]:


df [df['W'] > 0]['Y']


# In[52]:


df[df['W'] > 0] [['Y','X']]


# In[55]:


df [(df['W']>0) & (df['Y'] > 1)]


# In[56]:


# More index details
df


# In[57]:


# Reset to defult 0,1...n index
df.reset_index()


# In[61]:


newind = 'CA NY WY OR CO'.split()


# In[68]:


df ['States'] = newind


# In[69]:


df


# In[70]:


df.set_index ('States')


# In[71]:


df


# In[72]:


df.set_index('States',inplace = True)


# In[73]:


df


# In[79]:


# Multi-Index and Index Hierarchy
#index levels
outside = ['G1','G1','G1','G2','G2','G2']
inside = [1,2,3,1,2,3]
hier_index = list(zip(outside,inside))
hier_index = pd.MultiIndex.from_tuples(hier_index)


# In[80]:


hier_index


# In[82]:


df = pd.DataFrame (np.random.randn(6,2),index = hier_index,columns = ['A','B'])
df


# In[83]:


df.loc['G1']


# In[84]:


df.loc['G1'].loc[1]


# In[85]:


df.index.names


# In[86]:


df.index.names = ['Group','Num']


# In[87]:


df


# In[88]:


df.xs(['G1',1])


# In[89]:


df.xs(1,level = 'Num')


# In[102]:


#Missing Data
df = pd.DataFrame ({'A':[1,2,np.nan],
                   'B': [5,np.nan,np.nan],
                   'C': [1,2,3]})


# In[103]:


df


# In[96]:


df.dropna()


# In[98]:


df.dropna(axis=1)


# In[99]:


df.dropna(thresh =2)


# In[100]:


df.fillna(value = 'FILL VALUE')


# In[101]:


df['A'].fillna(value = df ['A'].mean())


# In[109]:


#Group by
# creat datafrane
data = {'Company':['GOOG','GOOG','MSFT','MSFT','FB','FB'],
       'Person': ['Sam','Charlie','Amy','Vanessa','Carl','Sarah'],
       'Sales': [200,120,340,124,243,350]}


# In[110]:


df = pd.DataFrame(data)


# In[111]:


df


# In[112]:


df.groupby('Company')


# In[113]:


by_comp = df.groupby('Company')


# In[114]:


by_comp.mean()


# In[115]:


df.groupby('Company').mean()


# In[116]:


by_comp.std()


# In[117]:


by_comp.min()


# In[118]:


by_comp.max()


# In[119]:


by_comp.count()


# In[121]:


by_comp.describe()


# In[129]:


#Mergin, join, and Concatenation
df1 = pd.DataFrame ({'A':['A0','A1','A2','A3'],
                    'B': ['B0','B1','B2','B3'],
                    'C': ['D0','D1','D2','D3']},
                   index = [0,1,2,3])


# In[130]:


df2 = pd.DataFrame ({'A':['A4','A5','A6','A7'],
                    'B': ['B4','B5','B6','B7'],
                    'C': ['D4','D5','D6','D7']},
                   index = [4,5,6,7])


# In[131]:


df3 = pd.DataFrame ({'A':['A8','A9','A10','A11'],
                    'B': ['B8','B9','B10','B11'],
                    'C': ['D8','D9','D10','D11']},
                   index = [8,9,10,11])


# In[132]:


df1


# In[133]:


df2


# In[134]:


df3


# In[135]:


#Concation
pd.concat ([df1,df2,df3])


# In[137]:


pd.concat([df1,df2,df3],axis = 1)


# In[184]:


# example dataFrames
left = pd.DataFrame({'key': ['K0', 'K1', 'K2', 'K3'],
                      'A': ['A0', 'A1', 'A2', 'A3'],
                      'B': ['B0', 'B1', 'B2', 'B3']})
 

right = pd.DataFrame({'key': ['K0', 'K1', 'K2', 'K3'],
                           'C': ['C0', 'C1', 'C2', 'C3'],
                           'D': ['D0', 'D1', 'D2', 'D3']}) 


# In[185]:


left


# In[168]:


right


# In[174]:


#Merging
pd.merge(left, right, how = 'inner', on='key')


# In[187]:


left = pd.DataFrame({'key1': ['K0', 'K0', 'K1', 'K2'],
                      'key2': ['K0', 'K1', 'K0', 'K1'],
                          'A': ['A0', 'A1', 'A2', 'A3'],
                          'B': ['B0', 'B1', 'B2', 'B3']})
 

right = pd.DataFrame({'key1': ['K0', 'K1', 'K1', 'K2'],
                               'key2': ['K0', 'K0', 'K0', 'K0'],
                                   'C': ['C0', 'C1', 'C2', 'C3'],
                                   'D': ['D0', 'D1', 'D2', 'D3']})


# In[188]:


pd.merge (left, right, on=['key1','key2'])


# In[190]:


pd.merge(left, right, how='outer', on=['key1', 'key2'])


# In[191]:


pd.merge(left, right, how='right', on=['key1', 'key2'])


# In[192]:


pd.merge(left, right, how='left', on=['key1', 'key2'])


# In[195]:


# Joining
left = pd.DataFrame({'A': ['A0', 'A1', 'A2'],
                     'B': ['B0', 'B1', 'B2']},
                      index=['K0', 'K1', 'K2'])

right = pd.DataFrame({'C': ['C0', 'C2', 'C3'],
                    'D': ['D0','D2', 'D3']},
                      index=['K0','K2','K3'])


# In[196]:


left.join(right)


# In[197]:


left.join(right, how='outer')


# In[198]:


#operations
df = pd.DataFrame({'col1':[1,2,3,4],'col2':[444,555,666,444],'col3':['abc','def','ghi','wyz']})
df.head()


# In[199]:


#info on Unique Values
df ['col2'].unique()


# In[200]:


df['col2'].unique()


# In[201]:


df['col2'].value_counts()


# In[202]:


# selection data
#select from DataFrame using criteria from multipe columns
newdf = df[(df['col1']>2) & (df['col2']==444)]


# In[203]:


newdf


# In[204]:


#Applying Functions
def times2(x):
    return x*2


# In[206]:


df ['col1'].apply(times2)


# In[207]:


df['col3'].apply(len)


# In[208]:


df ['col1'].sum()


# In[209]:


#permanently Removing a Column
del df ['col1']


# In[210]:


df


# In[212]:


#get column and index names
df.columns


# In[213]:


df.index


# In[214]:


#sorting and Ordering a dataFrame
df


# In[215]:


# insplace = false by defult
df.sort_values(by='col2')


# In[216]:


#find null values or chech for null values
df.isnull()


# In[217]:


#drop rows with NaN Values
df.dropna()


# In[218]:


#filling in NaN values with something else
df = pd.DataFrame ({'col1':[1,2,3,np.nan],
                    'col2':[np.nan,555,666,444],
                    'col3':['abc','def','ghi','wyz']})
df.head()


# In[219]:


df.fillna('FILL')


# In[220]:


data = {'A':['foo','foo','foo','bar','bar','bar'],
     'B':['one','one','two','two','one','one'],
       'C':['x','y','x','y','x','y'],
       'D':[1,3,2,5,4,1]}

df = pd.DataFrame(data)


# In[221]:


df


# In[222]:


df.pivot_table(values='D',index=['A','B'],columns=['C'])


# In[2]:


#Data input and output
#input CSV
import numpy as np
import pandas as pd
df = pd.read_csv('example')
df


# In[3]:


#CSV output
df.to_csv('example',index=False)


# In[11]:


pip install xlrd


# In[13]:


import numpy as np
import pandas as pd
df = pd.read_excel('Excel_Sample.xlsx')
df


# In[14]:


df.to_excel('Excel_Sample.xlsx')


# In[1]:


pip install lxml


# In[1]:


pip install html5lib


# In[1]:


pip install BeautifulSoup4


# In[5]:


#HTML Input
import numpy as np
import pandas as pd

df = pd.read_html('http://www.fdic.gov/bank/individual/failed/banklist.html')


# In[6]:


df[0]


# In[ ]:




